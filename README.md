# Entity Updater

## Introduction

Mass updates entities via a queue. Provides drush command to add entities to
queue by entity type and bundle.

## Usage

### From custom code:

Add all entities of type and bundle to queue:

    EntityUpdater::create()->enqueueAll($entity_type, $bundle);

Add single entity to queue:

    EntityUpdater::create()->enqueueUpdate($entity);

    EntityUpdater::create()->enqueueUpdateById($entity_type_id, $id);

### Drush command

You can also use drush command to add all entities of type and bundle to queue:

    drush entity-updater:enqueue node page

## Recommendation

- use queue_ui module to see queue status or run batch queue execution;
- use drush to force queue processing and not wait for cron execution;
`drush queue-run entity_updater`

## Credits

* Initial development by Wolfgang Ziegler, drunomics GmbH <hello@drunomics.com>
