<?php

namespace Drupal\entity_updater\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Updates the enqueued entities.
 *
 * @QueueWorker(
 *   id = "entity_updater",
 *   title = @Translation("Update entites"),
 *   cron = {"time" = 30}
 * )
 */
class EntityUpdaterQueueWorker extends QueueWorkerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Gets the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager() {
    if (empty($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $entity = $this->getEntityTypeManager()->getStorage($data['type'])
      ->load($data['id']);

    // Noting to do if deleted meanwhile.
    if (!$entity) {
      return;
    }

    // If the entity was updated meanwhile, skip.
    if ($entity instanceof EntityChangedInterface && $entity->getChangedTime() > $data['created']) {
      return;
    }

    // Avoid creating a new revision.
    if ($entity instanceof RevisionableInterface) {
      $entity->setNewRevision(FALSE);
    }

    // Finally, update!
    $entity->save();

    // Free up memory by removing from cache.
    $this->getEntityTypeManager()->getStorage($data['type'])
      ->resetCache([$data['id']]);

  }

}
