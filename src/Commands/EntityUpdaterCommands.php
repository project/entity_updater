<?php

namespace Drupal\entity_updater\Commands;

use Drupal\entity_updater\EntityUpdater;
use Drush\Commands\DrushCommands;

/**
 * The Drush commandfile.
 */
class EntityUpdaterCommands extends DrushCommands {

  /**
   * Enqueue updates for all entities of the given type and bundle.
   *
   * @param string $entity_type
   *   The entity type to enqueue.
   * @param string $bundle
   *   (optional) The bundle.
   *
   * @usage entity-updater:enqueue node page
   *   Enqeueues all nodes for update.
   *
   * @command entity-updater:enqueue
   * @aliases entity_updater:enqueue,euq
   */
  public function enqueue($entity_type, $bundle = NULL) {
    $count = EntityUpdater::create()
      ->enqueueAll($entity_type, $bundle);

    $this->logger()->success(dt('%count entities have been enqueued.', ['%count' => $count]));
  }

}
