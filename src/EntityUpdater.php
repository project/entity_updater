<?php

namespace Drupal\entity_updater;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides utility methods to enqueue entity updates.
 */
class EntityUpdater {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Gets the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager() {
    if (empty($this->entityTypeManager)) {
      $this->entityTypeManager = \Drupal::entityTypeManager();
    }
    return $this->entityTypeManager;
  }

  /**
   * Creates a helper object.
   *
   * @return static
   */
  public static function create() {
    return new static();
  }

  /**
   * Enqueues an update for the given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function enqueueUpdate(EntityInterface $entity) {
    $this->enqueueUpdateById($entity->getEntityTypeId(), $entity->id());
  }

  /**
   * Enqueues an update for the given entity.
   *
   * @param string $entity_type_id
   *   THe entity type ID.
   * @param mixed $id
   *   The entity ID.
   */
  public function enqueueUpdateById($entity_type_id, $id) {
    $queue = \Drupal::queue('entity_updater', TRUE);
    $queue->createItem([
      'id' => $id,
      'type' => $entity_type_id,
      'created' => \Drupal::time()->getRequestTime(),
    ]);
  }

  /**
   * Enqueues updates for all entities of the given type and bundle.
   *
   * @param string $entity_type_id
   *   The entity type.
   * @param string $bundle
   *   (optional) The bundle to enqueue items for.
   *
   * @throws \InvalidArgumentException
   *   Thrown when an invalid entity type ID is given.
   *
   * @return int
   *   The number of entries enqueued.
   */
  public function enqueueAll($entity_type_id, $bundle = NULL) {
    $query = \Drupal::entityQuery($entity_type_id);
    try {
      $type = $this->getEntityTypeManager()->getDefinition($entity_type_id, FALSE);
    }
    catch (PluginNotFoundException $exception) {
      throw new \InvalidArgumentException("Invalid entity type given.");
    }
    if ($bundle) {
      $key = $type->getKey('bundle');
      $query->condition($key, $bundle);
    }
    $query->accessCheck();
    $ids = $query->execute();
    foreach ($ids as $id) {
      $this->enqueueUpdateById($entity_type_id, $id);
    }
    return count($ids);
  }

}
